class Brain < ApplicationRecord
    belongs_to :zombie

    validates :flavor, presence:true
    validates :Iq,numericality: {only_integer:true,message:"Solo numeros enteros"}
    
end
