class Zombie < ApplicationRecord
    has_many :brains
    mount_uploader :avatar, FotoUploader
    validates :bio, length: { maximum: 100}
    validates :name, presence: true
    validates :age, numericality:{ only_integer: true, message: "Sólo números enteros"}
    validates :email, format: { with: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i, message: "formato mal" }
    belongs_to :user
end
