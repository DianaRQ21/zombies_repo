Rails.application.routes.draw do
  get 'lista_usuarios/index'

  root to:"zombies#index"

devise_scope :admin do
  	get 'lista_usuarios/index'
  end  
    
  
devise_for :admins, controllers: {
	sessions: 'admins/sessions'
}

  devise_for :users, controllers: {
    session: 'users/session'
  }

  resources :zombies do
  resources :brains
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end


get '/appnuevis/cerebros', to:'brains#index', as:'brains'



end