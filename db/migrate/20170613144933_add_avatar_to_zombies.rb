class AddAvatarToZombies < ActiveRecord::Migration[5.0]
  def change
    add_column :zombies, :avatar, :String
  end
end
